﻿using System.Web;
using System.Web.Optimization;

namespace mvc
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/css/core.css",
                      "~/Content/css/slider.css"));

            bundles.Add(new ScriptBundle("~/bundles/angularfiles").Include(
                    "~/Content/js/homeapp.js",

                    "~/Content/js/factories/*.js",
                    "~/Content/js/controllers/*.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
