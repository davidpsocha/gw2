﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace mvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "daily",
               url: "daily",
               defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
               name: "worlds",
               url: "worlds",
               defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
               name: "map",
               url: "map",
               defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
               name: "crafting",
               url: "crafting",
               defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
               name: "profession",
               url: "profession",
               defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
               name: "about",
               url: "about",
               defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
               name: "googleseo",
               url: "google11c0c322ff4c7135.html",
               defaults: new { controller = "Home", action = "GoogleSEO" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
