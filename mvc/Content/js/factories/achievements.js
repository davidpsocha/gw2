﻿app.factory('AchievementFactory', ["$http", function($http){
    var factory = {};
    factory.getDailyInfo = function (json, gameType, maxLevel, successResult) {
        var idList = [];
        for (var i = 0; i < json.length; i++)
        {
            idList.push(json[i].id);
        }
        
        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/achievements?ids=' + idList.join(),
            // For full list go to: https://api.guildwars2.com/v2/achievements/categories?ids=1,2,3,4,5,6,7,10,11,13,14,16,22,23,25,26,27,28,29,30,31,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,65,66,67,68,69,70,71,72,73,74,75,76,77,78,80,81,82,83,88,97,99,100,101,102,104,108,109,110,111,112,114,116,117,118,119,121,122,123,124,125,126,127,129,130,131,132,133,134,136,137,138,139,141,142
            cache: true
        }).then(function successCallback(response) {
            var data = response.data;
            var result = [];
            
            // Loop through the results and match up against the daily json information passed in.
            for (var i = 0; i < data.length; i++)
            {
                var id = data[i].id;
                var shouldSave = false;
                for (var j = 0; j < json.length; j++)
                {
                    // Once we find matching ids, check which access is required for the daily.
                    if (id == json[j].id) {
                        var index = json[j].required_access.indexOf(gameType);
                        var min = json[j].level.min;
                        var max = json[j].level.max;
                        // Make sure the right expansion is selected and the max character level is within the threshold.
                        if (index >= 0 && min <= maxLevel && maxLevel <= max) {
                            shouldSave = true;
                        }
                    }
                }
                if(shouldSave)
                    result.push(data[i]);
            }
            successResult(result);
        }, function errorCallback(response) {
            console.error(response);
        });
    };

    factory.getBloodstoneDaily = function (successResult) {
        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/achievements?ids=3052,3083,3068,3055',
            cache: true
        }).then(function successCallback(response) {
            successResult(response.data);
        }, function errorCallback(response) {
            console.error(response);
        });
    };

    factory.getFractalDaily = function (successResult) {
        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/achievements?ids=2245,2266,2900,2898,2964,2956,2976,2955,2944,2903,2979,2985,2934,2966',
            cache: true
        }).then(function successCallback(response) {
            successResult(response.data);
        }, function errorCallback(response) {
            console.error(response);
        });
    };

    return factory;
}]);