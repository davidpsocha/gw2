﻿app.factory('RecipeFactory', ["$http", function ($http) {
    var factory = {};
    factory.getRecipeInfo = function (json, quality, startIndex, endIndex, successResult) {
        var idList = [];
        //console.log("start: " + startIndex + ", end: " + endIndex + ", total: " + json.length);
        for (var i = startIndex; i < json.length && i < endIndex; i++)
        {
            idList.push(json[i].output_item_id);
        }

        // If there are no ids to search through pass back and empty json result.
        if (idList.length <= 0)
        {
            var response = { data: [] };
            successResult(response);
            return;
        }

        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/items?ids=' + idList.join(),
            // Example result: https://api.guildwars2.com/v2/items?ids=28445,28446
            cache: true
        }).then(function successCallback(response) {
            var data = response.data;
            var result = [];
            
            // Loop through the results and match up against the recipe json information passed in.
            for (var i = 0; i < data.length; i++)
            {
                var id = data[i].id;
                var rarity = data[i].rarity;
                for (var j = 0; j < json.length; j++)
                {
                    if (id == json[j].output_item_id) {
                        result.push(data[i]);
                    }
                }
            }
            successResult(result);
        }, function errorCallback(response) {
            console.error(response);
        });
    };

    return factory;
}]);