﻿var app = angular.module("HomeApp", ["ngRoute", "ngCookies"]);
app.config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "/home/home"
    })
    .when("/daily", {
        templateUrl: "/home/daily"
    })
    .when("/worlds", {
        templateUrl: "/home/worlds"
    })
    .when("/map", {
        templateUrl: "/home/map"
    })
    .when("/crafting", {
        templateUrl: "/home/crafting"
    })
    .when("/profession", {
        templateUrl: "/home/profession"
    })
    .when("/about", {
        templateUrl: "/home/about"
    })
    .otherwise("/");

    $locationProvider.html5Mode(true);
}]);
app.controller("IndexController", ["$scope", "$rootScope", function ($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        // Setup link tracking
        jQuery(document).ready(function () {
            jQuery("a").each(function () {
                var href = $(this).attr("href");
                var target = $(this).attr("target");
                var text = $(this).text();
                jQuery(this).click(function (event) {
                    event.preventDefault();
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'link',
                        eventAction: 'click',
                        eventLabel: text
                    });
                    setTimeout(function () {
                        window.open(href, (!target ? "_self" : target));
                    }, 300);
                });
            });
        });
    });
}]);