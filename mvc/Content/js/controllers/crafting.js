﻿app.controller("CraftingController", ["$rootScope", "$scope", "$http", "$cookies", "RecipeFactory", function ($rootScope, $scope, $http, $cookies, RecipeFactory) {
    // Prep all of the controller variables. ----------------------------------------
    $scope.Math = window.Math;
    $scope.recipeList = [];
    $scope.resultWeapons = [];
    $scope.resultArmors = [];
    $scope.modalItemJSON = {};
    $scope.modalRecipeJSON = {};
    $scope.qualityRange = 2;
    $scope.qualityName = "Masterwork";
    $scope.pageMin = 0;
    $scope.pageMax = 100;
    $scope.filteredListMax = 0;
    $scope.searchFilter = "";
    $scope.category =
    {
        selected: "",
        list: ["Weapon", "Armor"]
    }
    $scope.weapons =
    {
        types: ["Axe", "Dagger", "Focus", "Greatsword", "Hammer", "Harpoon", "LongBow", "Mace",
        "Pistol", "Rifle", "Scepter", "Shield", "ShortBow", "Speargun", "Staff", "Sword", "Torch", "Trident", "Warhorn"],
        selected: ""
    }
    $scope.armor =
    {
        types: ["Helm", "Shoulders", "Coat", "Gloves", "Leggings", "Boots"],
        selected: ""
    }

    // Get the master recipe list from the server json. -----------------------------
    $http({
        method: 'GET',
        url: '../Content/json/recipes.json',
        cache: true
    }).then(function successCallback(response) {
        $scope.recipeList = response.data;
    }, function errorCallback(response) {
        console.error(response);
    });
    function getMasterList() {
        var json = ""
        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/recipes'
        }).then(function successCallback(response) {
            var ids = response.data;
            for (var i = 0; i < ids.length; i += 100) {
                var url = 'https://api.guildwars2.com/v2/recipes?ids=';
                for (var j = i; j < (i + 100) && j < ids.length; j++) {
                    if (j == (i + 99)) {
                        url += ids[j];
                    }
                    else {
                        url += ids[j] + ",";
                    }
                }
                $http({
                    method: 'GET',
                    url: url
                }).then(function successCallback(response) {
                    json += JSON.stringify(response.data);
                }, function errorCallback(response) {
                    console.error(response);
                });
            }

        }, function errorCallback(response) {
            console.error(response);
        });
    }

    // Rarity filtering logic. --------------------------------------------------------
    $scope.$watch('qualityRange', function () {
        switch($scope.qualityRange)
        {
            case "0":
                $scope.qualityName = "Basic";
                break;
            case "1":
                $scope.qualityName = "Fine";
                break;
            case "2":
                $scope.qualityName = "Masterwork";
                break;
            case "3":
                $scope.qualityName = "Rare";
                break;
            case "4":
                $scope.qualityName = "Exotic";
                break;
            case "5":
                $scope.qualityName = "Ascended";
                break;
            case "6":
                $scope.qualityName = "Legendary";
                break;
            default:
                break;
        }
    });

    // Watch event category change and compute the max items for pagination.---------------------
    $scope.$watch('category.selected', function () {
        var newType = $scope.category.selected;
        var recipeLength = $scope.recipeList.length;
        var count = 0;
        for(var i = 0; i < recipeLength; i++)
        {
            if ($scope.recipeList[i].type == newType)
            {
                count++;
            }
        }
        //console.log(count);
        $scope.filteredListMax = count;
    });
    
    // Handle the previous and next page button events.------------------------------
    $scope.pagePrev = function () {
        if ($scope.pageMin > 0)
        {
            $scope.pageMin -= 100;
        }
        if ($scope.pageMax > 100)
        {
            $scope.pageMax -= 100;
        }
        
        $scope.weaponChange();
        $scope.armorChange();
    }
    $scope.pageNext = function () {
        if ($scope.pageMin < $scope.filteredListMax - 100) {
            $scope.pageMin += 100;
        }
        if ($scope.pageMax < $scope.filteredListMax) {
            $scope.pageMax += 100;
        }
        
        $scope.weaponChange();
        $scope.armorChange();
    }

    // Functions to handle selection dropdown changes. ------------------------------------------------
    $scope.weaponChange = function ()
    {
        // If nothing is selected, dont try to search for data.
        if ($scope.weapons.selected == "")
        {
            $scope.resultWeapons = [];
            return;
        }

        var resultsJSON = [];
        jQuery.each($scope.recipeList, function (index, value)
        {
            // Example of data structure here: https://api.guildwars2.com/v2/recipes/7319
            if (value.type == $scope.weapons.selected) {
                resultsJSON.push(value);
            }
        });
        $scope.filteredListMax = resultsJSON.length;
        //console.log($scope.filteredListMax);
        RecipeFactory.getRecipeInfo(resultsJSON, $scope.qualityName, $scope.pageMin, $scope.pageMax, function (results) {
            $scope.resultWeapons = results;
        });
    }
    $scope.armorChange = function () {
        // If nothing is selected, dont try to search for data.
        if ($scope.armor.selected == "")
        {
            $scope.resultArmors = [];
            return;
        }

        var resultsJSON = [];
        jQuery.each($scope.recipeList, function (index, value) {
            // Example of data structure here: https://api.guildwars2.com/v2/recipes/7319
            if (value.type == $scope.armor.selected) {
                resultsJSON.push(value);
            }
        });
        $scope.filteredListMax = resultsJSON.length;
        //console.log($scope.filteredListMax);
        RecipeFactory.getRecipeInfo(resultsJSON, $scope.qualityName, $scope.pageMin, $scope.pageMax, function (results) {
            $scope.resultArmors = results;
        });
    }

    // When a recipe is clicked, update the modal information.------------------------------------
    $scope.recipeClick = function(item)
    {
        $scope.modalItemJSON = item;
        var recipes = $scope.recipeList;
        for (var i = 0; i < recipes.length; i++)
        {
            if(recipes[i].output_item_id == item.id)
            {
                $scope.modalRecipeJSON = recipes[i];
                for (var j = 0; j < $scope.modalRecipeJSON.ingredients.length; j++)
                {
                    $http({
                        method: 'GET',
                        url: 'https://api.guildwars2.com/v2/items/' + $scope.modalRecipeJSON.ingredients[j].item_id,
                        cache: true
                    }).then(function successCallback(response) {
                        for (var k = 0; k < $scope.modalRecipeJSON.ingredients.length; k++)
                        {
                            if ($scope.modalRecipeJSON.ingredients[k].item_id == response.data.id)
                            {
                                $scope.modalRecipeJSON.ingredients[k].name = response.data.name;
                            }
                        }
                    }, function errorCallback(response) {
                        console.error(response);
                    });
                }
                break;
            }
        }
    }

    // Handle ingredient detail loading.--------------------------------------------
    $scope.getIngredientInfo = function(id)
    {
        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/items/'+id,
            cache: true
        }).then(function successCallback(response) {
            return response.data.name;
        }, function errorCallback(response) {
            console.error(response);
        });
    }
}]);