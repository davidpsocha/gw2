﻿app.controller("DailyController", ["$scope", "$http", "$cookies", "AchievementFactory", function ($scope, $http, $cookies, AchievementFactory) {
    $scope.dailyPvE = [];
    $scope.dailyPvP = [];
    $scope.dailyWvW = [];
    $scope.dailyBloodstone = [];
    $scope.dailyFractal = [];
    $scope.gameTypeOwned = "GuildWars2";
    $scope.maxLevelCharacter = parseInt($cookies.get('maxLevelCharacter'));

    $scope.getDailies = function () {
        var expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + 30 * 86400000); // cookie expires in a month
        $cookies.put('maxLevelCharacter', $scope.maxLevelCharacter, { expires: expireDate }); 
        $http({
            method: 'GET',
            url: 'https://api.guildwars2.com/v2/achievements/daily',
            cache: true
        }).then(function successCallback(response) {
            AchievementFactory.getDailyInfo(response.data.pve, $scope.gameTypeOwned, $scope.maxLevelCharacter, function (result) {
                $scope.dailyPvE = result;
            });
            AchievementFactory.getDailyInfo(response.data.pvp, $scope.gameTypeOwned, $scope.maxLevelCharacter, function (result) {
                $scope.dailyPvP = result;
            });
            AchievementFactory.getDailyInfo(response.data.wvw, $scope.gameTypeOwned, $scope.maxLevelCharacter, function (result) {
                $scope.dailyWvW = result;
            });
        }, function errorCallback(response) {
            console.error(response);
        });

        AchievementFactory.getBloodstoneDaily(function (result) {
            $scope.dailyBloodstone = result;
        });

        AchievementFactory.getFractalDaily(function (result) {
            $scope.dailyFractal = result;
        });
    };

    $scope.getDailies();
}]);