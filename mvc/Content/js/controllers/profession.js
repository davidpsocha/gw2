﻿app.controller("ProfessionController", ["$scope", "$http", "$cookies", function ($scope, $http, $cookies) {
    $scope.professions = [];
    $scope.modalName = "";
    $scope.modalJSON = [];
    $scope.expertMode = $cookies.get('expertMode') == "true" ? true : false;

    $http({
        method: 'GET',
        url: 'https://api.guildwars2.com/v2/professions',
        cache: true
    }).then(function successCallback(response) {
        for (var i = 0; i < response.data.length; i++) {
            $http({
                method: 'GET',
                url: 'https://api.guildwars2.com/v2/professions/'+response.data[i],
                cache: true
            }).then(function successCallback(responseProfession) {
                $scope.professions.push(responseProfession.data);
            }, function errorCallback(responseProfession) {
                console.error(response);
            });
        }
    }, function errorCallback(response) {
        console.error(response);
    });

    $scope.profClicked = function(prof)
    {
        if(prof.class == "profSelected")
        {
            prof.class = "";
        }
        else
        {
            prof.class = "profSelected";
        }
    }

    $scope.weaponClicked = function(profName, key, weapon)
    {
        $scope.modalJSON = [];
        $scope.modalName = profName + " " + key;

        for (var i = 0; i < weapon.skills.length; i++) {
            $http({
                method: 'GET',
                url: 'https://api.guildwars2.com/v2/skills/' + weapon.skills[i].id,
                cache: true
            }).then(function successCallback(response) {
                $scope.modalJSON.push(response.data);
            }, function errorCallback(response) {
                console.error(response);
            });
        }
    }

    $scope.getFact = function(facts, type)
    {
        for(var i = 0; i < facts.length; i++)
        {
            if(facts[i].type == type)
            {
                return facts[i];
            }
        }
    }

    $scope.expertModeChange = function()
    {
        var expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + 30 * 86400000); // cookie expires in a month
        $cookies.put('expertMode', $scope.expertMode, { expires: expireDate });
    }
}]);