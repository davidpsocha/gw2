﻿app.controller("MapController", ["$scope", "$http", function ($scope, $http) {
    $scope.checkWaypoints = true;
    $scope.checkVistas = true;
    $scope.checkPois = false;

    // Code sample http://jsfiddle.net/cliff/CRRGC/
    // Other useful links:
    //https://wiki.guildwars2.com/wiki/API:2/maps
    //https://wiki.guildwars2.com/wiki/API:Tile_service
    //https://developers.google.com/maps/documentation/javascript/maptypes
    //http://leafletjs.com/reference.html
    //https://api.guildwars2.com/v2/files?ids=all

    var map;
    var MINZOOM = 2;
    var previousZoom = MINZOOM;

    var waypointList = [];
    var waypointIcon = L.icon({
        iconUrl: 'https://render.guildwars2.com/file/32633AF8ADEA696A1EF56D3AE32D617B10D3AC57/157353.png',
        iconRetinaUrl: 'https://render.guildwars2.com/file/32633AF8ADEA696A1EF56D3AE32D617B10D3AC57/157353.png',
        iconSize: [32, 32]
        //iconAnchor: [22, 94],
        //popupAnchor: [-3, -76],
        //shadowUrl: '../../Content/assets/waypoint.png',
        //shadowRetinaUrl: '../../Content/assets/waypoint.png',
        //shadowSize: [68, 95],
        //shadowAnchor: [22, 94]
    });
    var vistaList = [];
    var vistaIcon = L.icon({
        iconUrl: 'https://render.guildwars2.com/file/A2C16AF497BA3A0903A0499FFBAF531477566F10/358415.png',
        iconRetinaUrl: 'https://render.guildwars2.com/file/A2C16AF497BA3A0903A0499FFBAF531477566F10/358415.png',
        iconSize: [32, 32]
    });
    var poiList = [];
    var poiIcon = L.icon({
        iconUrl: 'https://render.guildwars2.com/file/25B230711176AB5728E86F5FC5F0BFAE48B32F6E/97461.png',
        iconRetinaUrl: 'https://render.guildwars2.com/file/25B230711176AB5728E86F5FC5F0BFAE48B32F6E/97461.png',
        iconSize: [32, 32]
    });

    function unproject(coord) {
        return map.unproject(coord, map.getMaxZoom());
    }

    $scope.updateMap = function (e) {
        // Only show waypoints if checked and lower than 3 zoom levels.
        if ($scope.checkWaypoints && map.getZoom() > 3) {
            for (var i = 0; i < waypointList.length; i++) {
                waypointList[i].setOpacity(1);
            }
        }
        else
        {
            for (var i = 0; i < waypointList.length; i++) {
                waypointList[i].setOpacity(0);
            }
        }

        // Only show vistas if checked and lower than 3 zoom levels.
        if ($scope.checkVistas && map.getZoom() > 3) {
            for (var i = 0; i < vistaList.length; i++) {
                vistaList[i].setOpacity(1);
            }
        }
        else {
            for (var i = 0; i < vistaList.length; i++) {
                vistaList[i].setOpacity(0);
            }
        }

        // Only show pois if checked and lower than 3 zoom levels.
        if ($scope.checkPois && map.getZoom() > 3) {
            for (var i = 0; i < poiList.length; i++) {
                poiList[i].setOpacity(1);
            }
        }
        else {
            for (var i = 0; i < poiList.length; i++) {
                poiList[i].setOpacity(0);
            }
        }
    }

    $scope.init = function() {
        "use strict";

        var southWest, northEast;

        map = L.map("map", {
            minZoom: MINZOOM,
            maxZoom: 7,
            crs: L.CRS.Simple,
            zoomControl: false
        }).setView([0, 0], 0);

        var zoomControl = L.control.zoom({
            position: 'topleft'
        }).addTo(map);

        // Remove href attribute to avoid AngularJS routing issues.
        $("a.leaflet-control-zoom-in").removeAttr("href");
        $("a.leaflet-control-zoom-in").addClass("cursor-override-link");
        $(".leaflet-control-zoom-out").removeAttr("href");
        $(".leaflet-control-zoom-out").addClass("cursor-override-link");

        southWest = unproject([0, 32768]);
        northEast = unproject([32768, 0]);

        map.setMaxBounds(new L.LatLngBounds(southWest, northEast));

        L.tileLayer("https://tiles.guildwars2.com/1/1/{z}/{x}/{y}.jpg", {
            minZoom: MINZOOM,
            maxZoom: 7,
            continuousWorld: true,
            maxBoundsViscosity: 1.0
        }).addTo(map);

        map.on('zoomend', $scope.updateMap);

        $.getJSON("https://api.guildwars2.com/v1/map_floor.json?continent_id=1&floor=1", function (data) {
            var region, gameMap, i, il, poi;

            for (region in data.regions) {
                region = data.regions[region];

                for (gameMap in region.maps) {
                    gameMap = region.maps[gameMap];

                    for (i = 0, il = gameMap.points_of_interest.length; i < il; i++) {
                        poi = gameMap.points_of_interest[i];

                        if (poi.type == "waypoint") {

                            var marker = L.marker(unproject(poi.coord), {
                                title: poi.name,
                                icon: waypointIcon,
                                opacity: 0
                            });
                            
                            waypointList.push(marker);
                            marker.addTo(map);
                        }
                        else if (poi.type == "vista") {

                            var marker = L.marker(unproject(poi.coord), {
                                title: poi.name,
                                icon: vistaIcon,
                                opacity: 0
                            });

                            vistaList.push(marker);
                            marker.addTo(map);
                        }
                        else if (poi.type == "landmark") {

                            var marker = L.marker(unproject(poi.coord), {
                                title: poi.name,
                                icon: poiIcon,
                                opacity: 0
                            });

                            poiList.push(marker);
                            marker.addTo(map);
                        }
                        else
                        {
                            //console.log(poi);
                        }
                    }
                }
            }
        });
    };
}]);