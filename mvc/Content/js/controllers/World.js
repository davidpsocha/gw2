﻿app.controller("WorldController", ["$scope", "$http", function ($scope, $http) {
    $scope.worlds = [];
    $scope.propertyName = "default";
    $scope.reverse = false;

    $http({
        method: 'GET',
        url: 'https://api.guildwars2.com/v2/worlds?ids=all'
    }).then(function successCallback(response) {
        $scope.worlds = response.data;
    }, function errorCallback(response) {
        console.error(response);
    });

    $scope.sortBy = function (propertyName) {
        $scope.reverse = $scope.propertyName == propertyName ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.controllerSort = function (world)
    {
        var prop = $scope.propertyName;
        if ($scope.propertyName == "population" || $scope.propertyName == "default")
        {
            switch (world.population) {
                case 'Medium':
                    return 1;

                case 'High':
                    return 2;

                case 'VeryHigh':
                    return 3;

                case 'Full':
                    return 4;
            }
        }
        else
        {
            return world.name;
        }
    }
}]);