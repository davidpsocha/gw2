﻿app.controller("HomeController", ["$scope", "$http", "$interval", function ($scope, $http, $interval) {

    $scope.currentCycle = "";
    $scope.nextCycle = "";
    $scope.timeRemaining = "";

    // Setup time intervals. Everything is represented in UTC.
    var dawn =
    {
        start: { hour: 0, min: 25 },
        end: { hour: 0, min: 30 }
    };
    var day =
    {
        start: { hour: 0, min: 30 },
        end: { hour: 1, min: 40 }
    };
    var dusk =
    {
        start: { hour: 1, min: 40 },
        end: { hour: 1, min: 45 }
    };
    var night =
    {
        start: { hour: 1, min: 45 },
        end: { hour: 0, min: 25 }
    };

    // Function to check if the time given is within the cycle given.
    function withinCycle(cycle, timeToTest)
    {
        // Night is a special case since it loops around the interval.
        if (cycle.start.hour == night.start.hour && cycle.start.min == night.start.min &&
            cycle.end.hour == night.end.hour && cycle.end.min == night.end.min)
        {
            return (timeToTest.hour == 0 && timeToTest.min < cycle.end.min) ||
                (timeToTest.hour == 1 && cycle.start.min <= timeToTest.min)
        }
            // Day is also a special case since it extends across an hour change.
        else if (cycle.start.hour == day.start.hour && cycle.start.min == day.start.min &&
                cycle.end.hour == day.end.hour && cycle.end.min == day.end.min) {
            return (timeToTest.hour == 0 && cycle.start.min <= timeToTest.min) ||
                (timeToTest.hour == 1 && timeToTest.min < cycle.end.min)
        }
        else {
            // All other cycles get processed here.
            return (cycle.start.hour == timeToTest.hour && cycle.start.min <= timeToTest.min)
                && (cycle.end.hour == timeToTest.hour && timeToTest.min < cycle.end.min);
        }
    }

    function getTimeRemaining(nextCycle)
    {
        var nowDate = new Date(2016, 01, 01, new Date().getUTCHours() % 2, new Date().getUTCMinutes(), new Date().getUTCSeconds());
        var endDate = new Date(2016, 01, 01, nextCycle.end.hour, nextCycle.end.min, 0);

        // For tickDiff, if the next interval loops around the to the next modulo hour, add two for the math
        // to be non negative for the countdown.
        var tickDiff = 0;
        if (nowDate.getHours() == 1 && endDate.getHours() == 0)
        {
            tickDiff = (endDate.setHours(endDate.getHours() + 2) - nowDate.getTime()) / 1000;
        }
        else
        {
            tickDiff = (endDate.getTime() - nowDate.getTime()) / 1000;
        }
        var hourDiff = Math.floor(tickDiff / 3600);
        var minuteDiff = Math.floor((tickDiff % 3600) / 60);
        var secondsDiff = tickDiff % 60;
        return hourDiff + ":" + pad(minuteDiff, 2) + ":" + pad(secondsDiff, 2);
    }

    function pad(n, width) {
        var n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
    }

    $interval(function () {
        var now =
        {
            hour: new Date().getUTCHours() % 2,
            min: new Date().getUTCMinutes()
        };

        if (withinCycle(dawn, now)) {
            $scope.currentCycle = "Dawn";
            $scope.nextCycle = "Day";
            $scope.timeRemaining = getTimeRemaining(dawn);
        }
        else if (withinCycle(day, now)) {
            $scope.currentCycle = "Day";
            $scope.nextCycle = "Dusk";
            $scope.timeRemaining = getTimeRemaining(day);
        }
        else if (withinCycle(dusk, now)) {
            $scope.currentCycle = "Dusk";
            $scope.nextCycle = "Night";
            $scope.timeRemaining = getTimeRemaining(dusk);
        }
        else if (withinCycle(night, now)) {
            $scope.currentCycle = "Night";
            $scope.nextCycle = "Dawn";
            $scope.timeRemaining = getTimeRemaining(night);
        }

    }, 1000);


    /*
    // Testing code block.
    var dawns = 0, days = 0, dusks = 0, nights = 0, unknowns = 0;
    for (var h = 0; h < 2; h++)
    {
        for(var m = 0; m < 60; m++)
        {
            var temp = { hour: h, min: m };
            if (withinCycle(dawn, temp)) {
                dawns++;
                //console.log("dawn");
            }
            else if (withinCycle(day, temp)) {
                days++;
                //console.log("day");
            }
            else if (withinCycle(dusk, temp)) {
                dusks++;
                //console.log("dusk");
            }
            else if (withinCycle(night, temp)) {
                nights++;
                //console.log("night");
            }
            else
            {
                unknowns++;
                //console.log("unknown");
            }
        }
    }
    console.log("Dawns: " + dawns);
    console.log("Days: " + days);
    console.log("Dusks: " + dusks);
    console.log("Nights: " + nights);
    console.log("Unknowns: " + unknowns);
    */
}]);