﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            var result = new FilePathResult("~/Views/Home/Home.cshtml", "text/html");
            return result;
        }

        public ActionResult Daily()
        {
            var result = new FilePathResult("~/Views/Home/Daily.cshtml", "text/html");
            return result;
        }

        public ActionResult Worlds()
        {
            var result = new FilePathResult("~/Views/Home/Worlds.cshtml", "text/html");
            return result;
        }

        public ActionResult Map()
        {
            var result = new FilePathResult("~/Views/Home/Map.cshtml", "text/html");
            return result;
        }

        public ActionResult Crafting()
        {
            var result = new FilePathResult("~/Views/Home/Crafting.cshtml", "text/html");
            return result;
        }

        public ActionResult Profession()
        {
            var result = new FilePathResult("~/Views/Home/Profession.cshtml", "text/html");
            return result;
        }

        public ActionResult About()
        {
            var result = new FilePathResult("~/Views/Home/About.cshtml", "text/html");
            return result;
        }

        /// <summary>
        /// Google SEO vertification file.
        /// </summary>
        /// <returns></returns>
        public ActionResult GoogleSEO()
        {
            var result = new FilePathResult("~/Views/Home/google11c0c322ff4c7135.html", "text/html");
            return result;
        }
    }
}